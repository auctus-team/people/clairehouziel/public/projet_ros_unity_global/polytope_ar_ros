# Présentation du workspace

Ce workspace comprend des packages préexistants de l'équipe:

- le package [panda_capacity](https://gitlab.inria.fr/auctus-team/people/antunskuric/ros/ros_nodes/panda_capacity) d'Antun (pour le calcul et l'affichage des polytopes)
- le package franka_ros du workspace [panda_qp_ws](https://gitlab.inria.fr/auctus-team/components/robots/panda/panda_qp_ws) de Lucas
- le repository [setup_calibration](https://gitlab.inria.fr/auctus-team/projects/internal/lego_manipulation/setup-calibration/-/tree/master/) de Lucas pour calibrer le robot par rapport au QR Code
- le repository [setup_description](https://gitlab.inria.fr/auctus-team/projects/external/solvay/glovebox/solvay_setup_description) en complément du précédent


Associés au package permettant de faire l'interface entre ROS et Unity :
- le repository [ROS TCP Endpoint](https://github.com/Unity-Technologies/ROS-TCP-Endpoint)

Et avec un nouveau package "panda_unity" créé pour le projet, composé :
- un node "**effector_position**" qui souscrit au joint_states et publie la position de l'effecteur par rapport à la base du robot dans le topic _/effector_coordinates_
- un node "**velocity_effector**" qui souscrit au joint_states et publie le vecteur vitesse de l'effecteur par rapport à l'effecteur dans le topic _/velocity_effector_
- un node "**cube_subscriber**" qui souscrit au script de Unity pour recevoir les coordonnées du cube le plus proche de l'effecteur

# Launch

Pour lancer le projet via le robot en interaction manuelle : 
`roslaunch panda_capacity one_panda_real.launch`

Pour lancer le projet via le robot en simulation seule (interaction via rviz): 
`roslaunch panda_capacity one_panda.launch` 
(l'hologramme ne sera donc pas calé sur le robot réel)

Pour lancer le projet via le robot en MotionPlanning via ROS : 
`roslaunch panda_capacity one_panda_motion.launch`




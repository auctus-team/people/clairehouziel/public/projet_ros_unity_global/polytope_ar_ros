#!/usr/bin/env python3
import rospy
import time
import numpy as np
import tf.transformations as tr
import pynocchio.RobotWrapper

#on utilise les joint states en temps réel
from sensor_msgs.msg import JointState
#on publie des coordonnées
from geometry_msgs.msg import Pose


# initial joint positions
q = [0,0,0,0,0,0,0]
# loading the root urdf from robot_description parameter
robot = pynocchio.RobotWrapper(xml_path=rospy.get_param("/panda/robot_description"))

# function receiving the new joint positions
def callback(data):
    global q
    q = data.position

def effector_position():
    global q
    rospy.init_node('effector_position', anonymous=True)
    sub = rospy.Subscriber('/panda/joint_states', JointState, callback)
    pub = rospy.Publisher('effector_coordinates', Pose, queue_size=3)

    rate = rospy.Rate(10) # 10hz
    while not rospy.is_shutdown():
        pose = Pose()
        #T la transformation homogène, p la translation, quat le quaternion
        T = np.array(robot.forward(q))
        
        #on applique la rotation pour retrouver les axes du robot dans Unity
        Tru= np.array([[0,0,1,0],[-1,0,0,0],[0,1,0,0], [0,0,0,1]])

        #on fait Tru ^-1 (robot-unity) * Tre (robot - effecteur) pour obtenir Tue (unity - effecteur)
        Tue = np.linalg.pinv(Tru).dot(T)

        # on en déduit la position de l'effecteur par rapport à la base du robot Unity et son quaternion
        quat = tr.quaternion_from_matrix(Tue)
        p = Tue[0:3,3]

        pose.position.x, pose.position.y, pose.position.z =p[0], p[1], p[2]
        pose.orientation.x, pose.orientation.y, pose.orientation.z, pose.orientation.w =quat[0], quat[1], quat[2], quat[3]
        rospy.loginfo(pose)
        pub.publish(pose)
        rate.sleep()

if __name__ == '__main__':
    effector_position()


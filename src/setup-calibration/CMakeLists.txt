cmake_minimum_required(VERSION 2.8.3)
project(setup_calibration)

find_package(catkin REQUIRED COMPONENTS rospy )

catkin_package()

catkin_python_setup()


#!/usr/bin/env python3

import numpy as np
import math
import tf2_ros
import rospy
from tf import transformations as tf
import yaml
import rospkg
import sys

def TFToNumpy(transform):
    # Transform a tf to a numpy array
    trans = tf.translation_matrix(
        (
            transform.transform.translation.x,
            transform.transform.translation.y,
            transform.transform.translation.z,
        )
    )
    rot = tf.quaternion_matrix(
        (
            transform.transform.rotation.x,
            transform.transform.rotation.y,
            transform.transform.rotation.z,
            transform.transform.rotation.w,
        )
    )
    pose = tf.concatenate_matrices(trans, rot)
    return pose


if __name__ == "__main__":
    if len(sys.argv) < 2:
        print("usage: calibrate.py name")
        quit()
    else:
        name = sys.argv[1]
    # Initialize node and objects
    rospy.init_node("world_tag_calibration")
    rospack = rospkg.RosPack()
    tfBuffer = tf2_ros.Buffer()
    listener = tf2_ros.TransformListener(tfBuffer)

    # Grab parameters or use default value
    child_frame_id_param = rospy.get_param("~child_frame_id", "calibration_tag")
    tag_length_param = rospy.get_param("~tag_length", 0.10)

    # Fill A matrix (see math in Readme)
    L = tag_length_param / 2.0
    A = np.array(
        [
            [1, 0, 0, 0, -L * math.sqrt(2)],
            [0, 1, 0, -L * math.sqrt(2), 0],
            [0, 0, 1, 0, 0],
            [1, 0, 0, -L * math.sqrt(2), 0],
            [0, 1, 0, 0, L * math.sqrt(2)],
            [0, 0, 1, 0, 0],
            [1, 0, 0, 0, L * math.sqrt(2)],
            [0, 1, 0, L * math.sqrt(2), 0],
            [0, 0, 1, 0, 0],
            [1, 0, 0, L * math.sqrt(2), 0],
            [0, 1, 0, 0, -L * math.sqrt(2)],
            [0, 0, 1, 0, 0],
        ]
    )

    # Grab points
    input("Acquire upper left corner. Press Enter to continue...")
    p1_tf = tfBuffer.lookup_transform("world", "panda/pointer_tip", rospy.Time())
    p1 = TFToNumpy(p1_tf)[(0, 1, 2), (3, 3, 3)]
    print(p1)
    input("Acquire upper right corner. Press Enter to continue...")
    p2_tf = tfBuffer.lookup_transform("world", "panda/pointer_tip", rospy.Time())
    p2 = TFToNumpy(p2_tf)[(0, 1, 2), (3, 3, 3)]
    print(p2)
    input("Acquire lower right corner. Press Enter to continue...")
    p3_tf = tfBuffer.lookup_transform("world", "panda/pointer_tip", rospy.Time())
    p3 = TFToNumpy(p3_tf)[(0, 1, 2), (3, 3, 3)]
    print(p3)
    input("Acquire lower left corner. Press Enter to continue...")
    p4_tf = tfBuffer.lookup_transform("world", "panda/pointer_tip", rospy.Time())
    p4 = TFToNumpy(p4_tf)[(0, 1, 2), (3, 3, 3)]
    print(p4)

    # Concatenate points in a single column vector
    p = np.concatenate([p1, p2, p3, p4])

    # Compute solution = A^# p
    sol = np.linalg.pinv(A).dot(p)

    # Compute theta from arctangent and substract pi/4
    theta = math.pi / 4.0 - math.atan2(sol[3], sol[4])
    print(theta)

    #Translation du centre vers le point supérieur gauche
    sol[0]-= L*math.sqrt(2)*math.sin(math.pi/4 - theta)
    sol[1]-= L*math.sqrt(2)*math.cos(math.pi/4 - theta)
    
    # Convert it into quaternion to match geometry_msg/Transform convention
    q = tf.quaternion_from_euler(0.0, 0.0, theta)

    # #Convert quaternion in rotation matrix
    # Trq = tf.quaternion_matrix(q)

    # #Add the translation to transform matrix
    # Trq[0,3], Trq[1,3], Trq[2,3] = sol[0], sol[1], sol[2]

    # #Compute transform from regular robot to Unity robot 
    # Tru= np.array([[0,0,1,0],[0,-1,0,0],[1,0,0,0],[0,0,0,1]])

    # #Calculate the transform from QRCode to Unity Robot
    # Tqu = Tru.dot(Trq)

    # #Inversion of x and y to resolve the conversion right to left-handed
    # Rquright = Tqu [0:3,0:3]
    # Rquleft = Rquright.dot(np.array([[0,1,0],[1,0,0],[0,0,1]]))
    # pqu = Tqu[0:3,3]
    # pquleft = np.array([[pqu[1]], [pqu[0]], [pqu[2]]])

    # Tquleft = np.concatenate((Rquleft,pquleft), axis = 1)
    # Tquleft = np.concatenate((Tquleft,np.array([[0,0,0,1]])), axis = 0 )

    # sol = pquleft
    # q = tf.quaternion_from_matrix(Tquleft)

    # Fill a dict object with all the information
    calib_dat = dict(
        header=dict(frame_id="world", child_frame_id=name),
        position=dict(x=float(sol[0]), y=float(sol[1]), z=float(sol[2])),
        orientation=dict(x=float(q[0]), y=float(q[1]), z=float(q[2]), w=float(q[3])),
        angle =dict(theta=float(theta))
    )
    print(calib_dat)
    # Store into a yaml file
    with open(
        rospack.get_path("setup_calibration")
        + "/config/"
        + name
        + ".yaml",
        "w",
    ) as file:
        yaml.dump(calib_dat, file)

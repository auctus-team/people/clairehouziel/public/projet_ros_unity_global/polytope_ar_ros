#!/usr/bin/env python3

from numpy import average
import rospy
import math
import tf2_ros
import geometry_msgs.msg
from tf import transformations as tf
import yaml
import rospkg

def loadTransformFromYaml(yaml_path):
    transform = geometry_msgs.msg.TransformStamped()
    with open(yaml_path) as f:
        data = yaml.load(f, Loader=yaml.FullLoader)
        transform.header.stamp = rospy.Time.now()
        transform.header.frame_id = data['header']['frame_id']
        transform.child_frame_id = data['header']['child_frame_id']
        transform.transform.translation.x =  data['position']['x']
        transform.transform.translation.y =  data['position']['y']
        transform.transform.translation.z =  data['position']['z']
        transform.transform.rotation.x =  data['orientation']['x']
        transform.transform.rotation.y =  data['orientation']['y']
        transform.transform.rotation.z =  data['orientation']['z']
        transform.transform.rotation.w =  data['orientation']['w']
    return transform

if __name__ == '__main__':
    # Initialize node and objects
    rospy.init_node('tf2_listener')
    rate = rospy.Rate(10.0)
    rospack = rospkg.RosPack()
    tfBuffer = tf2_ros.Buffer()
    listener = tf2_ros.TransformListener(tfBuffer)
    br = tf2_ros.TransformBroadcaster()
    static_broadcaster = tf2_ros.StaticTransformBroadcaster()
     
    # Load calibration tag position relatively to the world
    tf_qr = loadTransformFromYaml(rospack.get_path('setup_calibration')+'/config/qr_code.yaml')
    # tf_board_left = loadTransformFromYaml(rospack.get_path('setup_calibration')+'/config/board_left.yaml')
    # tf_board_right = loadTransformFromYaml(rospack.get_path('setup_calibration')+'/config/board_right.yaml')
    # tf_r_center = loadTransformFromYaml(rospack.get_path('setup_calibration')+'/config/red.yaml')
    # tf_g_center = loadTransformFromYaml(rospack.get_path('setup_calibration')+'/config/green.yaml')
    # tf_b_center = loadTransformFromYaml(rospack.get_path('setup_calibration')+'/config/blue.yaml')
    # tf_y_center = loadTransformFromYaml(rospack.get_path('setup_calibration')+'/config/yellow.yaml')

    tf_shared_stack_center = loadTransformFromYaml(rospack.get_path('setup_calibration')+'/config/shared_stack.yaml')

    static_broadcaster.sendTransform([tf_qr])
    rospy.spin()
